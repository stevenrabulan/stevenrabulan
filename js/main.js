$(document).ready(function() {
  //Position img.portrait-overflow based on .main-heading's height
  var overflowPortrait = function() {
  	var dynamicMargin = $('.main-heading').height();
		$('.portrait-overflow').delay(500).css({'margin-top':-dynamicMargin});
    //Give height to spacer (only visible on x-small devices) based on portrait's negative margin
    $('.portrait-overflow-spacer').delay(500).css({'height':dynamicMargin});
  };
  overflowPortrait();
    
  //Collapse navbar on link click
  $('.navbar-nav').on('click', function() {
    $('.in.navbar-collapse').collapse('hide');
  });
  
  //Enable bootstrap scrollspy with 70px offset (fixed top navbar)
  var fixedNavOffset = 70;
  $('body').scrollspy({ target: '#navMain', offset: (fixedNavOffset + 1) });
  
  //Define scrollspyRefresh
  var scrollspyRefresh = function() {
    $('body').each(function () {
      var $spy = $(this).scrollspy('refresh');
      });
    };
  
  //Animate same page anchors that start with "#"
  $("a[href^=#]").on("click", function(){
     var target = $($(this).attr("href"));
     // If target exists...
     if(target[0]){
         // Calculate position to scroll to, either target or bottom of document
         var y = Math.min(target.offset().top - fixedNavOffset, $(document).height() - $(window).height());
         // Disable scrolling to negative values
         y=Math.max(y,0);
         // Animate the scroll
         $("html,body").stop().animate({ "scrollTop": y }, 3000, 'easeOutQuint');
     }
     return false;
  });
  
  //User scroll will cancel any scroll animation
  $("body,html").bind("scroll mousedown DOMMouseScroll mousewheel keyup", function(e){
    if ( e.which > 0 || e.type === "mousedown" || e.type === "mousewheel"){
         $("html,body").stop();
    }
  });
          
  //Lazy load youtube iframes
  //When clicking youtube thumbnails...
  $('.youtube-thumbnail,.youtube-play').click(function() {
    //Move it behind the video iframe and use absolute positioning so parent ignores thumbnail dimensions
    $(this).siblings('.youtube-thumbnail').css({
      "position": "absolute",
      "z-index": "5"
    });
    //Unhide iframe and bring forward
    $(this).siblings('iframe.youtube-embed-responsive').css({
      "display": "block",
      "z-index": "10"
    });
    //Load stored src url
    $(this).siblings('iframe.youtube-embed-responsive').attr('src', function() {
        return $(this).data('src');
    });
    //Add padding to youtube video parent to match thumbnail
    $(this).parent().css({
      "padding-bottom": "67.5%",
      "padding-top": "25px"
    });
  });
  //Store src to prevent loading
  $('iframe.youtube-embed-responsive').each(function() {
      var src = $(this).attr('src');
      $(this).data('src', src).attr('src', '');
  });
          
  //Show Bootstrap tooltips
  $("[data-toggle='tooltip']").tooltip({animation: true});
  
  //Show Bootstrap modals
  $('.img-modal-enabled').on('click', function() {
    $('#modalGlobal').modal({
      show: true,
    });
    //Use img's "alt" attribute as modal header
    var myHeader = $(this).attr('alt');
    $('#modalHeader').text(myHeader);
    //Use img's hidden, adjacent ".modal-caption" content as modal caption
    var myCaption = $(this).siblings('.modal-caption').html();
    $('#modalCaption').html(myCaption);
    //Take clicked img's src and remove "-TN.xxx" and add .jpg to be modal image's source
    var mySrc = (this.src.substr(0, this.src.length-7) + '.jpg');
    $('#modalImage').attr('src', mySrc);
    //Exit modal on click
    $('#modalImage').on('click', function(){
      $('#modalGlobal').modal('hide');
    });
  });
  
  //When modal opens, scroll to top
  $('#modalGlobal').on('shown.bs.modal', function () {
      $('#modalGlobal').animate({ scrollTop: 0 }, 'fast');
  });

  //Parallax scroll, 3d transform method (uses GPU, faster)
  $(window).scroll(function(event) {
    // note: most browsers presently use prefixes: webkitTransform, mozTransform etc.
    var parallax = $('img#parallax-background');
    var parallaxCoords = ($(window).scrollTop()/-20);
    var parallaxStyles = {'transform': 'translate3d(0px,' + parallaxCoords + 'px, 0px)'}
    parallax.css(parallaxStyles);
  });
  
  //Old parallax scroll, css method (slower)
  /*
  var $window = $(window);

  $('body[data-type="background"]').each(function(){
    var $bgobj = $(this); // assigning the object
    $(window).scroll(function() {
      var yPos = -($window.scrollTop() / $bgobj.data('speed'));
      // Put together our final background position
      var coords = '50% '+ yPos + 'px';
      // Move the background
      $bgobj.css({ backgroundPosition: coords}); 
    });
  });
  */
  
  //Custom easing, taken from JQuery UI
  $.extend($.easing,
  {
      def: 'easeOutQuad',
      easeOutQuint: function (x, t, b, c, d) {
          return c*((t=t/d-1)*t*t*t*t + 1) + b;
      }
  });
  
  //Trigger resize on orientation change event (minified)
  /*! A fix for the iOS orientationchange zoom bug.
   Script by @scottjehl, rebound by @wilto.
   MIT / GPLv2 License.
  */
  (function(w){var ua=navigator.userAgent;if(!(/iPhone|iPad|iPod/.test(navigator.platform)&&/OS [1-5]_[0-9_]* like Mac OS X/i.test(ua)&&ua.indexOf("AppleWebKit")>-1)){return;}var doc=w.document;if(!doc.querySelector){return;}var meta=doc.querySelector("meta[name=viewport]"),initialContent=meta&&meta.getAttribute("content"),disabledZoom=initialContent+",maximum-scale=1",enabledZoom=initialContent+",maximum-scale=10",enabled=true,x,y,z,aig;if(!meta){return;}function restoreZoom(){meta.setAttribute("content",enabledZoom);enabled=true;}function disableZoom(){meta.setAttribute("content",disabledZoom);enabled=false;}function checkTilt(e){aig=e.accelerationIncludingGravity;x=Math.abs(aig.x);y=Math.abs(aig.y);z=Math.abs(aig.z);if((!w.orientation||w.orientation===180)&&(x>7||((z>6&&y<8||z<8&&y>6)&&x>5))){if(enabled){disableZoom();}}else if(!enabled){restoreZoom();}}w.addEventListener("orientationchange",restoreZoom,false);w.addEventListener("devicemotion",checkTilt,false);})(this);
  
  //Orientation Change Triggers Resize Event
  $(window).bind('orientationchange', function() {
    console.log('Orientation Change');
    $(window).resize();
  });
  
  //Perform these functions on resize
  $(window).resize(function() {
    overflowPortrait();
    scrollspyRefresh();
  }).resize(); // Trigger resize handlers

}); //All DOM-ready JQuery Loaded

