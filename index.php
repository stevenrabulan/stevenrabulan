<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Steven Rabulan: Business-minded web developer</title>

<!--<link rel="shortcut icon" href="favicon.ico">
<link rel="icon" href="favicon.ico">-->
<link rel="stylesheet" href="lib/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" href="lib/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/main.css">
<link href='https://fonts.googleapis.com/css?family=Domine:400,700|Oxygen:400,700' rel='stylesheet' type='text/css'>
<script> <!-- asynchronous google analytics tracking code -->
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-44022807-1', 'stevenrabulan.com');
	ga('send', 'pageview');

</script>
</head>

<body data-type="background" data-speed="6">
	<img class="parallax-background" id="parallax-background" src="css/i/wood-bg.jpg">
<?php include('includes/header.php'); ?>

<div class="container" id="pagewrap">

	<div class="row row-heading">
		<section class="col col-sm-8 col-sm-offset-4 main-heading" id="Home" >
			<h1>Steven Rabulan is a business-minded web designer &amp; developer based in <span class="nowrap">Evanston, IL.<span></h1>
		</section>
	</div> <!-- /.row-heading -->

	<div class="row row-main">
		<section class="content-home col col-sm-8 col-sm-push-4">
			<div class="panel" id="About-Me">
				<div class="panel-body">
					<h3>Professional Experience</h3>
					<p>In 2012, I graduated from University of Illinois at Chicago with a Bachelor's in Marketing. During my academics, I consulted to several firms on social media marketing and market research. Upon completion of my degree, I hopped on-board at <a href="//inwk.com">InnerWorkings</a> as an <strong>eCommerce Implementation Specialist</strong> with primary roles in e-commerce store management and front-end development.</p>
					<p>Right now, I am a <strong>Front-End .NET Web Developer</strong> for <a href="//craftjack.com" target="blank">CraftJack, Inc.</a> I implement designs spanning our business operations including high traffic landing pages, a popular home improvement blog, marketing e-mails &amp; a web app.</p>
					<p>On the weekends, I operate <a href="https://www.clubcircuitdj.com" target="blank">Club Circuit DJ</a>, my professional wedding DJ business. CBS featured my business as (one of the) <a href="https://chicago.cbslocal.com/top-lists/best-djs-for-your-event-in-chicago/" target="blank">"Best DJs For Your Event In Chicago"</a>. Club Circuit Wedding DJ maintains a <a href="https://www.yelp.com/biz/club-circuit-wedding-dj-chicago-2" target="_blank">5-star average rating on <span class='symbol'>yelp</span>Yelp</a>.</p>
					<h3>Work Philosophy</h3>
					<p>In my experience, I've seen "new" web practices become obsolete rather quickly, so I've taken a meta approach to self-development. I'm adapted to learn new systems as opposed to relying on one particular method.</p>
					<p>I like driving my committed projects all the way to completion. I'm inclined to step out of my "assigned role" to make sure everything is handled.</p>
				</div>
			</div>
		</section> <!-- /.content-home -->
		<section class="portrait-overflow-spacer visible-xs"></section>
		<section class="content-sidebar col col-sm-4 col-sm-pull-8">
			<div class="panel">
				<div class="panel-body">
					<img class="img-responsive img-main portrait-overflow" width="400" height="438" src="images/steve-portrait-400-bw-transbg.png" alt="Steven Rabulan Portrait">
					<small class="text-small text-center">Freelance Web Designer &amp; Developer
						<br>Front-End .NET Web Developer, <a href="https://www.craftjack.com" target="_blank">CraftJack</a>
						<br>Owner-Operator, <a href="https://www.clubcircuitweddingdj.com" target="_blank">Club Circuit Wedding DJ</a>
					</small>
					<h3>Recent Work</h3>
					<ul class="work-featured-container">
						<li class="work-featured-item">
							<img class="img-responsive img-modal-enabled" width="480" height="135" src="images/portfolio/Recent-Work-BioSpawn-TN.jpg" alt="BioSpawn">
							<h4>BioSpawn</h4>
							<div class="work-featured-item-info modal-caption">
								<p><a href="https://www.biospawn.com" target="_blank" class="btn btn-primary btn-lg btn-launch">Launch Site</a><br>An online device-responsive storefront built on shopify.com platform. Built custom theme from scratch, based on designer comps.</p>
							<ul>
								<li>Entire Theme Development</li>
								<li>Shopify Integration</li>
								<li>E-mail Newsletter Template</li>
								<li>Product Photography</li>
							</ul>
							</div>
						</li>
						<li class="work-featured-item">
							<img class="img-responsive img-modal-enabled" width="480" height="135" src="images/portfolio/Recent-Work-CraftJack-TN.jpg" alt="CraftJack">
							<h4>CraftJack</h4>
							<div class="work-featured-item-info modal-caption">
									<p><a href="https://www.craftjack.com" target="_blank" class="btn btn-primary btn-lg btn-launch">Launch Site</a><br>Implemented designs into device-responsive code for main corporate site and trade blog.</p>
								<ul>
									<li>Front-End Site Development</li>
									<li>Original animations</li>
									<li>Split testing using Optimizely</li>
								</ul>
							</div>
						</li>
						<li class="work-featured-item">
							<img class="img-responsive img-modal-enabled" width="480" height="135" src="images/portfolio/Recent-Work-OfficeMax-TN.jpg" alt="OfficeMax">
							<h4>OfficeMax</h4>
							<div class="work-featured-item-info modal-caption"> 
								<p>Client wanted to closely match our eCommerce solution to the look &amp; feel of past vendor's platform, in order to reduce user frustration. I used HTML and CSS to mimic their previous interface, given the limitations of a table-based layout.</p>
							</div>
						</li>
						<li class="work-featured-item">
							<img class="img-responsive img-modal-enabled" width="480" height="135" src="images/portfolio/Recent-Work-InterContinental-TN.jpg" alt="InterContinental Hotels">
							<h4>InterContinental</h4>
							<div class="work-featured-item-info modal-caption">
								<p>Responsible for static, Adobe Photoshop mockups and conversion into HTML and CSS3 on legacy, table-based eCommerce platform.</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="panel-footer panel-footer-flush"><a href="#Portfolio" class="btn btn-primary btn-block">More Examples <span class="glyphicon glyphicon-chevron-right"></span></a></div>
			</div> 
		</section> <!-- /.content-sidebar -->
	</div> <!-- /.row-main -->

	<div class="row row-bottom">
		<section class="content-portfolio col col-sm-12" id="Portfolio">
			<div class="panel">
				<div class="panel-body">
					<div class="row">
						<div class="col col-sm-12">
					
					<h3>Web Design &amp; Development</h3>
					<p>I&apos;ve had the pleasure of working with some very successful brands throughout my corporate experience. Sometimes, I'd be asked to produce visual mockup (Photoshop) to help land a big deal. Many of my projects involved skinning an e-commerce platform to match a client's look &amp; feel (HTML &amp; CSS) based on brand guidelines. You may view several work examples in this section.</p>
					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="509" src="images/portfolio/Portfolio-BioSpawn-TN.png" alt="BioSpawn.com">
							<div class="modal-caption">
								<p><a href="https://www.biospawn.com" target="_blank" class="btn btn-primary btn-lg btn-launch">Launch Site</a><br>An online device-responsive storefront built on shopify.com platform. Built custom theme from scratch, based on designer comps.</p>
							<ul>
								<li>Entire Theme Development</li>
								<li>Shopify Integration</li>
								<li>E-mail Newsletter Template</li>
								<li>Product Photography</li>
							</ul>
							
							</div>
						</div>
						<div class="col col-md-4">
							<h4>BioSpawn Lure Company</h4>
							<h5>Front-End Development, Shopify Theme</h5>
							<p>An online device-responsive storefront built on shopify.com platform. Built custom theme from scratch, based on designer comps.</p>
							<ul>
								<li>Entire Theme Development</li>
								<li>Shopify Integration</li>
								<li>E-mail Newsletter Template</li>
								<li>Product Photography</li>
							</ul>
							<a href="https://www.biospawn.com" target="_blank" class="btn btn-block btn-primary btn-lg">Launch Site</a>
						</div>
					</div>

					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="509" src="images/portfolio/Portfolio-CraftJack-TN.png" alt="CraftJack Corporate Site">
							<div class="modal-caption">
								<p><a href="https://www.craftjack.com" target="_blank" class="btn btn-primary btn-lg btn-launch">Launch Site</a><br>Implemented designs into device-responsive code for main corporate site and trade blog.</p>
							<ul>
								<li>Front-End Site Development</li>
								<li>Original animations</li>
								<li>Split testing using Optimizely</li>
							</ul>
							
							</div>
						</div>
						<div class="col col-md-4">
							<h4>CraftJack Corporate Site</h4>
							<h5>.NET Website, PHP Wordpress Blog</h5>
							<p>Implemented designs into device-responsive code for main corporate site and trade blog.</p>
							<ul>
								<li>Front-End Site Development</li>
								<li>Original animations</li>
								<li>Split testing using Optimizely</li>
							</ul>
							<a href="https://www.craftjack.com" target="_blank" class="btn btn-block btn-primary btn-lg">Launch Site</a>
						</div>
					</div>

					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="1142" height="550" src="images/portfolio/Portfolio-CCDJ-TN.png" alt="Club Circuit DJ Responsive Website">
							<div class="modal-caption">
								<p><a href="https://www.clubcircuitdj.com" target="_blank" class="btn btn-primary btn-lg btn-launch">Launch Site</a><br>A responsive website for my business. Fully designed &amp; developed by Steve Rabulan, involving:</p>
								<ul>
									<li>Site Design</li>
									<li>CSS3 <small>(&amp; SASS)</small>, HTML5, Javascript/JQuery, PHP and AJAX</li>
									<li>Marketing Copy</li>
									<li>Photography</li>
								</ul>
							
							</div>
						</div>
						<div class="col col-md-4">
							<h4>Club Circuit Wedding DJ</h4>
							<h5>Complete Site Design, Development &amp; Content</h5>
							<p>A responsive website for my business. Fully designed &amp; developed by Steve Rabulan, involving:</p>
							<ul>
								<li>Site Design</li>
								<li>CSS3 <small>(&amp; SASS)</small>, HTML5, Javascript/JQuery, PHP and AJAX</li>
								<li>Marketing Copy</li>
								<li>Photography</li>
							</ul>
							<a href="https://www.clubcircuitdj.com" target="_blank" class="btn btn-block btn-primary btn-lg">Launch Site</a>
						</div>
					</div>
				
					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="509" src="images/portfolio/Portfolio-JamesHardie-TN.png" alt="JamesHardie"/>
							<div class="modal-caption">
								<p>Created a landing page for quick user redirection to appropriate sites. Produced mobile-friendly PHP registration form (with client/server side validation) for new partner registrations. Responsible for static, Adobe Photoshop mockups and conversion into HTML and CSS3 on legacy, table-based eCommerce platform.</p>
							</div>
						</div>
						<div class="col col-md-4">
							<h4>JamesHardie</h4>
							<h5>E-Commerce Theme, Landing Page &amp; Registration Form</h5>
							<p>Created a landing page for quick user redirection to appropriate sites. Produced mobile-friendly PHP registration form (with client/server side validation) for new partner registrations. Responsible for static, Adobe Photoshop mockups and conversion into HTML and CSS3 on legacy, table-based eCommerce platform.</p>
						</div>
					</div>
				
					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="509" src="images/portfolio/Portfolio-FedEx-TN.png" alt="FedEx"/>
							<div class="modal-caption">
								<p>Responsible for Adobe Photoshop mockup of eCommerce store, used in a next-day sales presentation.</p>
							</div>
						</div>
						<div class="col col-md-4">
							<h4>FedEx</h4>
							<h5>E-Commerce Theme Mockup</h5>
							<p>Responsible for Adobe Photoshop mockup of eCommerce store, used in a next-day sales presentation.</p>
						</div>
					</div>
				
					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="508" src="images/portfolio/Portfolio-OfficeMax-TN.png" alt="OfficeMax"/>
							<div class="modal-caption">
								<p>Client wanted to closely match our eCommerce solution to the look &amp; feel of past vendor's platform, in order to reduce user frustration. I used HTML and CSS to mimic their previous interface, given the limitations of a table-based layout.</p>
							</div>
						</div>
						<div class="col col-md-4">
							<h4>OfficeMax</h4>
							<h5>E-Commerce Theme</h5>
							<p>Client wanted to closely match our eCommerce solution to the look &amp; feel of past vendor's platform, in order to reduce user frustration. I used HTML and CSS to mimic their previous interface, given the limitations of a table-based layout.</p>
						</div>
					</div>
				
					<div class="row project-container">
						<div class="col col-md-8">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="509" src="images/portfolio/Portfolio-InterContinental-TN.png" alt="InterContinental"/>
							<div class="modal-caption">
								<p>Responsible for static, Adobe Photoshop mockups and conversion into HTML and CSS3 on legacy, table-based eCommerce platform.</p>
							</div>
						</div>
						<div class="col col-md-4">
							<h4>InterContinental Hotels</h4>
							<h5>E-Commerce Theme</h5>
							<p>Responsible for static, Adobe Photoshop mockups and conversion into HTML and CSS3 on legacy, table-based eCommerce platform.</p>
						</div>
					</div>
				
					<div class="row project-container">
						<div class="col col-md-4">
							<img class="project-image img-responsive img-modal-enabled" width="285" height="356" src="images/portfolio/Portfolio-OneHope-TN.png" alt="One Hope Customizable Book"/>
							<div class="modal-caption">
								<p>My role was to build an online, step-by-step interface (HTML, JavaScript &amp; CSS) for users to customize the contents of their books on an existing e-commerce platform. I used Pageflex to accept user's customizations and generate a proof, as well as print-ready artwork to be sent off to the publisher. Their e-commerce solution was also designed to match their brand look &amp; feel.</p>
							</div>
						</div>
						<div class="col col-md-8">
							<h4>One Hope</h4>
							<h5>Customizable Book User Interface &amp; Template, E-Commerce Theme</h5>
							<p>My role was to build an online, step-by-step interface (HTML, JavaScript &amp; CSS) for users to customize the contents of their books on an existing e-commerce platform. I used Pageflex to accept user's customizations and generate a proof, as well as print-ready artwork to be sent off to the publisher. Their e-commerce solution was also designed to match their brand look &amp; feel.</p>
						</div>
					</div>
				
					<div class="row project-container">
						<div class="col col-md-4">
							<img class="project-image img-responsive img-modal-enabled" width="600" height="710" src="images/portfolio/Portfolio-Kindle-Cover-MeatNutBreakfast-TN.png" alt="Kindle Book Cover"/>
							<div class="modal-caption">
								<p>I designed an Amazon Kindle book cover using Photoshop, Illustrator and some stock images.</p>
							</div>
						</div>
						<div class="col col-md-8">
							<h4>Kindle eBook Author</h4>
							<h5>Front Cover Design</h5>
							<p>I designed an Amazon Kindle book cover using Photoshop, Illustrator and some stock images.</p>
						</div>
					</div>

					<h3>Clients</h3>
					<p>For my clients&apos; best interests, I've kept some of their work private. If you would like to see this work, I&apos;d be happy to share it with you one-on-one.</p>
					<div class="logo-grid well clearfix">
						<div class="logo-grid-item">
							<img width="64" height="64" src="images/portfolio/logos/logo-hp.png" alt="Hewlett Packard" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="42" height="64" src="images/portfolio/logos/logo-att.png" alt="AT&amp;T" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="53" height="64" src="images/portfolio/logos/logo-taco-bell.png" alt="Taco Bell" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="74" height="64" src="images/portfolio/logos/logo-ace.png" alt="ACE Insurance Group" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="177" height="22" src="images/portfolio/logos/logo-ferguson.png" alt="Ferguson" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="287" height="64" src="images/portfolio/logos/logo-newell-rubbermaid.png" alt="Newell Rubbermaid" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="170" height="64" src="images/portfolio/logos/logo-crowne-plaza.png" alt="Crowne Plaza" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="68" height="64" src="images/portfolio/logos/logo-echo.png" alt="Echo Global Logistics" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="240" height="35" src="images/portfolio/logos/logo-gannett.png" alt="Gannett" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="109" height="64" src="images/portfolio/logos/logo-college-hunks-hauling-junk.png" alt="College Hunks Hauling Junk" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="250" height="144" src="images/portfolio/logos/logo-hotel-indigo.gif" alt="Hotel Indigo" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="91" height="64" src="images/portfolio/logos/logo-holiday-inn.png" alt="Holiday Inn" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="154" height="64" src="images/portfolio/logos/logo-office-depot.png" alt="Office Depot" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="157" height="28" src="images/portfolio/logos/logo-lenox.png" alt="Lenox Tools" class="img-responsive" />
							<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="139" height="64" src="images/portfolio/logos/logo-success-academy.png" alt="Success Academy" class="img-responsive" />
						<div class="caption"></div>
						</div>
						<div class="logo-grid-item">
							<img width="287" height="53" src="images/portfolio/logos/logo-discover-boating.png" alt="Discover Boating" class="img-responsive" />
							<div class="caption"></div>
						</div>
			
					</div><!-- /.logo-grid -->
				</div>
			</div>

				
					<h3>Music Production</h3>
					<p>My music, featured in BuzzFeed.com videos:</p>
				
					<div class="row">
						<div class="col col-md-4">
							<div class="flex-video">
								<img class="img-responsive youtube-thumbnail" width="480" height="360" src="https://img.youtube.com/vi/5HiZSVtDHzs/0.jpg">
								<span class="youtube-play glyphicon glyphicon-play"></span>
								<iframe width="560" height="315" class="youtube-embed-responsive" src="https://www.youtube.com/embed/5HiZSVtDHzs?autoplay=1" frameborder="0" allowfullscreen></iframe>
							</div>
							<h5 class="flex-video-caption">Steve Rabulan - Nothing's Gonna Stop Me Instrumental</h5>
						</div>
						<div class="col col-md-4">
							<div class="flex-video">
								<img class="img-responsive youtube-thumbnail" width="480" height="360" src="https://img.youtube.com/vi/dYihakMcBMs/0.jpg">
								<span class="youtube-play glyphicon glyphicon-play"></span>
								<iframe width="560" height="315" class="youtube-embed-responsive" src="https://www.youtube.com/embed/dYihakMcBMs?autoplay=1" frameborder="0" allowfullscreen></iframe>
							</div>
							<h5 class="flex-video-caption">Steve Rabulan - SuperDope Instrumental</h5>
						</div>
						<div class="col col-md-4">
							<div class="flex-video">
								<img class="img-responsive youtube-thumbnail" width="480" height="360" src="https://img.youtube.com/vi/edc_pCBIOkw/0.jpg">
								<span class="youtube-play glyphicon glyphicon-play"></span>
								<iframe width="560" height="315" class="youtube-embed-responsive" src="https://www.youtube.com/embed/edc_pCBIOkw?autoplay=1" frameborder="0" allowfullscreen></iframe>
							</div>
							<h5 class="flex-video-caption">Steve Rabulan - Metamorphosis Instrumental</h5>
						</div>
					</div>
				
					<p>In 2012, I decided to release 33 of my beats with a "free-to-use" license. Since then, my music has been downloaded over 700 times and used in hundreds of youtube videos where my work is credited. The 33 songs can be found on <a href="https://soundcloud.com/steve-rabulan" target="_blank"><span class='symbol'>soundcloud</span>SoundCloud: Steve Rabulan</a>.</p>
				
				</div>
			
			</div>
		</section> <!-- /.portfolio -->
		<section class="content-contact col col-sm-12" id="Contact">
			<div class="panel">
				<div class="panel-body">
					<div class="row">
						<div class="col col-sm-12">
							<p>
								<a href="mailto:steven.rabulan@gmail.com">
								<span class="fa-stack">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope fa-inverse fa-stack-1x"></i>
								</span> Email</a>
								<br><a href="https://bitbucket.org/stevenrabulan/stevenrabulan/" target="_blank">
								<span class="fa-stack">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-bitbucket fa-inverse fa-stack-1x"></i>
								</span> BitBucket (portfolio source code)</a>
								<br><a href="https://www.linkedin.com/in/stevenrabulan" target="_blank">
								<span class="fa-stack">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-linkedin fa-inverse fa-stack-1x"></i>
								</span> LinkedIn</a>
								<br><a href="https://www.soundcloud.com/steve-rabulan" target="_blank">
								<span class="fa-stack">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-soundcloud fa-inverse fa-stack-1x"></i>
								</span> Soundcloud</a>
								<br><a href="https://www.yelp.com/biz/club-circuit-wedding-dj-chicago-2" target="_blank">
								<span class="fa-stack">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-yelp fa-inverse fa-stack-1x"></i>
								</span> Club Circuit Wedding DJ on Yelp!</a>
							</p>
						
						</div>
					</div>
				</div>
			</div>
		</section> <!-- /.contact -->
	</div> <!-- /.row-bottom -->

</div><!-- /#pagewrap-->

<?php include('includes/footer.php'); ?>	

<!-- jQuery -->
<script src="lib/jquery/dist/jquery.min.js"></script>
<!-- ><script src="//code.jquery.com/jquery.js"></script> -->
<!-- Bootstrap (jQuery Dependent) -->
<script src="lib/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Custom Scripts (jQuery Dependent) -->
<script src="js/main.js"></script>

</body>
</html>
