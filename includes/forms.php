<section class="ac-container">
	<div class="ac-separator">
		<input class="hidden" id="optionPricing" name="accordion-1" type="radio"/>
		<label class="btn btn-block btn-large mainUserAction" for="optionPricing">I need a DJ, what are your prices?<i class="icon icon-chevron-right pull-right"></i></label>
		<article class="ac-large">
			<form class="userform optiongPricingForm">
				<fieldset>
					<legend>Fill in the blanks below, and you'll be able to view our pricing. We have a variety of packages to customize. Wedding coverage <b>starts at $1,150.</b></legend>
					<div class="control-group">
						<label class="control-label" for="date">Date of Event</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-calendar"></i></span>
								<input type="date" id="date" name="date" placeholder="07/07/2014">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="name">Name</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-user"></i></span>
								<input type="text" id="name" name="name" placeholder="Jane Smith">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="email">Email</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-envelope"></i></span>
								<input type="email" id="email" name="email" placeholder="jsmith@gmail.com">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="phone">Phone</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-mobile-phone"></i></span>
								<input type="tel" id="phone" name="phone" placeholder="(773) 555-5555">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="details">Additional Details</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-info"></i></span>
								<input type="text" id="details" name="details" placeholder="~250 Guests, Downtown Chicago, etc.">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="human">(Security) What is 3 + 4?</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-question"></i></span>
								<input type="text" id="human" name="human" placeholder="Answer?">
							</div>
						</div>
					</div>
					<div class="result"></div>
					<button type="submit" name="submit" class="btn btn-large btn-primary pull-right formsubmit">Submit <i class="icon-ok
					icon-white"></i></button>
				</fieldset>
			</form>
		</article>
	</div><!--/optionPricing 1-->
	
	<div class="ac-separator">
		<input class="hidden" id="optionAvailability" name="accordion-1" type="radio"/>
		<label class="btn btn-block btn-large btn-primary mainUserAction" for="optionAvailability">Are you available for my event?<i class="icon icon-chevron-right pull-right"></i></label>
		<article class="ac-large">
			<form class="userform optionAvailabilityForm">
				<fieldset>
					<legend>We have limited availability throughout the year. Tell me a little bit about your event below and we can move toward locking down your date!</legend>
					<div class="control-group">
						<label class="control-label" for="date2">Date of Event</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-calendar"></i></span>
								<input type="date" id="date2" name="date2" placeholder="07/07/2014">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="name2">Name</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-user"></i></span>
								<input type="text" id="name2" name="name2" placeholder="Jane Smith">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="email2">Email</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-envelope"></i></span>
								<input type="email" id="email2" name="email2" placeholder="jsmith@gmail.com">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="phone2">Phone</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-mobile-phone"></i></span>
								<input type="tel" id="phone2" name="phone2" placeholder="(773) 555-5555">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="details2">Additional Details</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-info"></i></span>
								<input type="text" id="details2" name="details2" placeholder="~250 Guests, Downtown Chicago, etc.">
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="human2">(Security) What is 3 + 4?</label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-question"></i></span>
								<input type="text" id="human2" name="human2" placeholder="Answer?">
							</div>
						</div>
					</div>
					<div class="result"></div>
					<button type="submit" name="submit" class="btn btn-large btn-primary pull-right formsubmit">Submit <i class="icon-ok
				icon-white"></i></button>
				</fieldset>
			</form>
		</article>
	</div><!--/availabilityOption-->
</section>