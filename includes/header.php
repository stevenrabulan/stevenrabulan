<!-- #navMain -->
<header class="navbar navbar-default navbar-transluscent navbar-fixed-top" id="navMain" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="index.php"><strong>Steven</strong>Rabulan</a>
	  </div>
<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a href="#Home"><span>Home</span></a></li>
			<li><a href="#About-Me"><span>About Me</span></a></li>
			<li><a href="#Portfolio"><span>Portfolio</span></a></li>
			<li><a href="#Contact"><span>Contact</span></a></li>
		</ul>
	</div>
</header>
<!-- /#navMain -->

<!-- #modalGlobal -->
<div class="modal modal-responsive fade" id="modalGlobal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title" id="modalHeader"></h3>
      </div>
      <div class="modal-body">
        <div id="modalCaption"></div>
        <img id="modalImage" class="img-responsive" src="" alt="Modal Photo">
      </div>
      <div class="modal-footer modal-footer-flush">
        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /#modalGlobal -->